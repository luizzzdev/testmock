package main;

import java.util.ArrayList;
import java.util.List;

public class LinhaPedido {
	private int quantidade;
	private Produto produto;
	
	public LinhaPedido(Produto produto) {
		this.produto = produto;
	}
	

	public double calcularPreco() {
		return produto.obterPreco(quantidade);
	}

}
