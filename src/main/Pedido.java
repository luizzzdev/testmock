package main;

import java.util.List;

public class Pedido {
	List<LinhaPedido> linhaPedidos;
	Cliente cliente = new Cliente();
	
	public Pedido(List<LinhaPedido> linhaPedidos, Cliente cliente) {
		this.linhaPedidos = linhaPedidos;
		this.cliente = cliente;
	}
	
	public double calcularPreco() {
		double precoSemDesconto = 0;
		
		for(LinhaPedido linhaPedido : linhaPedidos) {
			precoSemDesconto += linhaPedido.calcularPreco();
		}
			
		double percentualDesconto = cliente.getPercentualDesconto();
		
		double precoComDesconto = precoSemDesconto;
		
		if (percentualDesconto > 0) {
			precoComDesconto = precoSemDesconto * (1 - percentualDesconto / 100);
		}
		
		if(precoComDesconto < 0) return 0;
		
		return precoComDesconto;
	}
	
}
