package main;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PedidoTest {

	private Pedido pedido;
	
	private List<LinhaPedido>  linhaPedidos = new ArrayList<>();
	
	@Mock
	private LinhaPedido  linhaPedido;
	
	@Mock
	private Cliente cliente;	
	
	@Before
	public void initPedido() {
		MockitoAnnotations.initMocks(this);
		linhaPedidos.add(linhaPedido);
		pedido = new Pedido(linhaPedidos, cliente);
	}
	
	@Test
	public void testaPedidoSemDesconto() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(100.0);	
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(0.0);
		
		assertEquals(pedido.calcularPreco(), 100.0);
	}
	
	@Test
	public void testaPedidoComDesconto() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(100.0);
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(10.0);
		
		assertEquals(pedido.calcularPreco(), 90.0);
	}
	
	@Test
	public void testaPedido100PorcentoDesconto() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(100.0);
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(100.0);
		
		assertEquals(pedido.calcularPreco(), 0.0);
	}
	
	@Test
	public void testaPedido200PorcentoDesconto() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(100.0);
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(200.0);
		
		assertEquals(pedido.calcularPreco(), 0.0);
	}
	
	@Test
	public void testaPedidoDescontoNegativo() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(100.0);
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(-5.0);
		
		assertEquals(pedido.calcularPreco(), 100.0);
	}
	
	@Test
	public void testaPedidoComLinhaPedidoZerada() {
		Mockito.when(linhaPedido.calcularPreco()).thenReturn(0.0);
		Mockito.when(cliente.getPercentualDesconto()).thenReturn(30.0);
		
		assertEquals(pedido.calcularPreco(), 0.0);
	}
}
